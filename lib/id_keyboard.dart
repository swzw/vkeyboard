import 'package:flutter/material.dart';

import 'cool_ui.dart';

class IDKeyboard extends StatelessWidget {
  static const CKTextInputType inputType = const CKTextInputType(name: 'CKNumberKeyboard');
  static final height = 40.0;
  static final doneHeight = 20.0;
  static final divide = 2.0;
  static double width = 30.0;
  static double getHeight(BuildContext? ctx) {
    if(ctx == null) {
      return 0;
    }
    MediaQueryData mediaQuery = MediaQuery.of(ctx);
    final size = mediaQuery.size;
    width = size.width / 3 - divide;
    return (height + divide) * 4 + doneHeight + divide;
  }

  final KeyboardController? controller;
  const IDKeyboard({this.controller});

  static register() {
    CoolKeyboard.addKeyboard(
        IDKeyboard.inputType,
        KeyboardConfig(
            builder: (context, controller, params) {
              return IDKeyboard(controller: controller);
            },
            getHeight: IDKeyboard.getHeight));
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    return Material(
      child: DefaultTextStyle(
          style: TextStyle(fontWeight: FontWeight.w500, color: Colors.black, fontSize: 23.0),
          child: Container(
            height: getHeight(context),
            width: mediaQuery.size.width,
            decoration: BoxDecoration(
              color: Color(0xFFE2E2E2),
            ),
            child: Column(children: [
              Container(
                height: doneHeight,
                width: mediaQuery.size.width,
                color: Color(0xFFd3d6dd),
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: Center(
                    child: Icon(Icons.expand_more),
                  ),
                  onTap: () {
                    controller!.doneAction();
                  },
                ),
              ),
              SizedBox(
                height: divide,
              ),
              Row(
                children: ['1', '2', '3'].map((e) => buildButton(e, height, width)).toList(),
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              ),
              SizedBox(
                height: divide,
              ),
              Row(
                children: ['4', '5', '6'].map((e) => buildButton(e, height, width)).toList(),
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              ),
              SizedBox(
                height: divide,
              ),
              Row(
                children: ['7', '8', '9'].map((e) => buildButton(e, height, width)).toList(),
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              ),
              SizedBox(
                height: divide,
              ),
              Row(
                children: ['0', 'X'].map((e) => buildButton(e, height, width)).toList()
                  ..add(Container(
                    height: height,
                    width: width,
                    color: Color(0xFFd3d6dd),
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Icon(Icons.backspace),
                      ),
                      onTap: () {
                        controller!.deleteOne();
                      },
                    ),
                  )),
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              ),
              SizedBox(
                height: divide,
              )
            ]),
          )),
    );
  }

  Widget buildButton(String title, double h, double w, {String? value}) {
    if (value == null) {
      value = title;
    }
    return Container(
      height: h,
      width: w,
      color: Colors.white,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        child: Center(
          child: Text(title),
        ),
        onTap: () {
          controller!.addText(value!);
        },
      ),
    );
  }
}
